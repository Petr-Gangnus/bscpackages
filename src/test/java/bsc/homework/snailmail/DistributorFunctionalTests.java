package bsc.homework.snailmail;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Test
public class DistributorFunctionalTests {

    @Test
    public void testLaunchThreadsSamePostSlowestOutputWithFees() throws FileNotFoundException {
        String resultFile = "output20.txt";
        String correctResultFile = "src/test/resources/correctOutput20.txt";
        Distributor distributor = Distributor.create("src/main/resources/fees.txt","src/test/resources/input20.txt", resultFile);
        distributor.launchThreads(20, 300);
        assertSameFiles(resultFile, correctResultFile);
    }
    @Test
    public void testLaunchThreadsSamePostSlowestOutputWithFeesAndLargeWeights() throws FileNotFoundException {
        String resultFile = "output21.txt";
        String correctResultFile = "src/test/resources/correctOutput21.txt";
        Distributor distributor = Distributor.create("src/main/resources/fees.txt","src/test/resources/input21.txt", resultFile);
        distributor.launchThreads(20, 300);
        assertSameFiles(resultFile, correctResultFile);
    }
    @Test
    public void testLaunchThreadsSamePostFastestOutputWithoutFees() throws FileNotFoundException {
        String resultFile = "output1.txt";
        String correctResultFile = "src/test/resources/correctOutput1.txt";
        Distributor distributor = Distributor.create(null,"src/test/resources/input1.txt", resultFile);
        distributor.launchThreads(20, 5);
        assertSameFiles(resultFile, correctResultFile);
    }
    @Test
    public void testLaunchThreadsSamePostSlowestOutputWithoutFees() throws FileNotFoundException {
        String resultFile = "output2.txt";
        String correctResultFile = "src/test/resources/correctOutput2.txt";
        Distributor distributor = Distributor.create(null,"src/test/resources/input1.txt", resultFile);
        distributor.launchThreads(20, 300);
        assertSameFiles(resultFile, correctResultFile);
    }
    @Test
    public void testLaunchThreadsSamePostGrowingFrequencyWithoutFees() throws FileNotFoundException {
        String resultFile = "output30.txt";
        String correctResultFile = "src/test/resources/correctOutput30.txt";
        Distributor distributor = Distributor.create(null,"src/test/resources/input30.txt", resultFile);
        distributor.launchThreads(50, 500);
        assertSameFiles(resultFile, correctResultFile);
    }
    @Test
    public void testLaunchThreadsSamePostFastestHalvedByIntervalOutputWithoutFees() throws FileNotFoundException {
        String resultFile = "output3.txt";
        String correctResultFile = "src/test/resources/correctOutput3.txt";
        Distributor distributor = Distributor.create(null,"src/test/resources/input3.txt", resultFile);
        distributor.launchThreads(10, 140);
        assertSameFiles(resultFile, correctResultFile);
    }
    @Test
    public void testLaunchThreadsDifferentPostsSlowestOutputWithoutFees() throws FileNotFoundException {
        String resultFile = "output10.txt";
        String correctResultFile = "src/test/resources/correctOutput10.txt";
        Distributor distributor = Distributor.create(null,"src/test/resources/input10.txt", resultFile);
        distributor.launchThreads(20, 300);
        assertResultLinesFoundInCorrectFile(resultFile, correctResultFile);
    }

    void assertSameFiles(String resultFileName, String correctResultFileName) throws FileNotFoundException {
        File resultFile = new File(resultFileName);
        Scanner resultScan = null;
        resultScan = new Scanner(resultFile);

        File correctFile = new File(correctResultFileName);
        Scanner correctScan;
        correctScan = new Scanner(correctFile);

        int lineNumber=0;
        while(resultScan.hasNextLine() ) {
            lineNumber++;
            Assert.assertTrue(correctScan.hasNextLine(), "The result output file has excessive line " + lineNumber);
            String resultLine = resultScan.nextLine();
            String correctLine = correctScan.nextLine();
            Assert.assertEquals(resultLine, correctLine, "Incorrect line number " + lineNumber + ",\n" + resultLine + "\ninstead of\n" + correctLine + "\n");
        }
        Assert.assertFalse(correctScan.hasNextLine(), "The result output file preliminary ends on line " + lineNumber+ "\n");
    }
    void assertResultLinesFoundInCorrectFile(String resultFileName, String correctResultFileName) throws FileNotFoundException {
        File resultFile = new File(resultFileName);
        Scanner resultScan = null;
        resultScan = new Scanner(resultFile);

        File correctFile = new File(correctResultFileName);
        Scanner correctScan;
        correctScan = new Scanner(correctFile);

        List<String> correctLines = new ArrayList<String>();
        List<String> resultLines = new ArrayList<String>();

        int lineNumber = 0;
        while(resultScan.hasNextLine() ) {
            lineNumber++;

            Assert.assertTrue(correctScan.hasNextLine(), "The result output file has excessive line " + lineNumber + "\n");
            resultLines.add(resultScan.nextLine());
            correctLines.add( correctScan.nextLine());
        }
        Assert.assertFalse(correctScan.hasNextLine(), "The result output file preliminary ends on line " + lineNumber + "\n");

        for (String line:resultLines){
            Assert.assertTrue(correctLines.contains(line),"result line '" + line + "' does not belong to the theoretical list\n");
        }

    }
}