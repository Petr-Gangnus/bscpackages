package bsc.homework.snailmail;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class OfficeUnitTests {

    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = "null pid")
    public void testNullPid() throws Exception{
        Office.createFromString(null);
    }
    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = ".*incorrect pid format")
    public void testEmptyPid() throws Exception{
        Office.createFromString("");
    }
    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = ".*incorrect pid format")
    public void testLongPid() throws Exception{
        Office.createFromString("111111");
    }
    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = ".*incorrect pid format")
    public void testShortPid() throws Exception{
        Office.createFromString("1111");
    }
    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = ".*incorrect pid format")
    public void testNoDigitPid() throws Exception{
        Office.createFromString("1111A");
    }
    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = ".*incorrect pid format")
    public void testDoubleWithSpacePid() throws Exception{
        Office.createFromString("11111 11111");
    }
    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = ".*incorrect pid format")
    public void testBrokenBySpacePid4() throws Exception{
        Office.createFromString("11 11");
    }
    @Test(expectedExceptions = { Exception.class })
    public void testBrokenByLinePid5() throws Exception{
        Office.createFromString("11\n111");
    }
    @Test(expectedExceptions = { Exception.class }, expectedExceptionsMessageRegExp = ".*incorrect pid format")
    public void testBrokenBySpacePid5() throws Exception{
        Office.createFromString("11 111");
    }
    @Test
    public void testGotPid() throws Exception{
        String pid = "11111";
        Office office = Office.createFromString(pid);
        Assert.assertEquals(office.toString(), pid, "Written and read pid does not fit with the source");
    }

}