package bsc.homework.snailmail;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class AllItemsUnitTests {
    @Test
    public void addAndGetEmpty(){
        AllItems items = new AllItems();
        items.append(null);
        List<Item> list = items.getAllTillEnd();
        Assert.assertEquals(list.size(), 0, "The number of new items read from the empty storage should be 0\n");
    }
    @Test
    public void addAndGetTwoAfterOne(){
        AllItems items = new AllItems();
        items.append(Item.createFromString("0.01 10000"));
        List<Item> list = items.getAllTillEnd();
        Assert.assertEquals(list.size(), 1, "The number of new items read from the fresh 1-item storage should be 1\n");
        items.append(Item.createFromString("0.02 20000"));
        items.append(Item.createFromString("0.03 30000"));
        list = items.getAllTillEnd();
        Assert.assertEquals(list.size(), 2, "The number of new items read from storage after 2 added items should be 2\n");
    }

}