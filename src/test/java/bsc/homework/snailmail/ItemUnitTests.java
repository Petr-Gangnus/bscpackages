package bsc.homework.snailmail;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;

import static org.testng.Assert.*;

@Test
public class ItemUnitTests {
    // Store the original standard err before changing it.
    private final PrintStream originalStdErr = System.err;
    private ByteArrayOutputStream consoleErrContent = new ByteArrayOutputStream();

    @Test
    public void testNormLine(){
        Item item = Item.createFromString("1 10000");
        assertEquals(item.getWeight(), new BigDecimal(1), "Weight is read incorrectly");
        assertEquals(item.getOffice().toString(), "10000", "Office is read incorrectly");
    }
    @Test
    public void testIncorrectPid() {
        System.setErr(new PrintStream(consoleErrContent));
        Item item = Item.createFromString("1 1000");
        assertNull(item, "Erroneous line should lead to null item.\n");
        assertTrue(consoleErrContent.toString().contains("incorrect pid format"),"Line wih bad pid should cause the output of the stack containing 'incorrect pid format'\n");
    }
    @Test(expectedExceptions = { NumberFormatException.class })
    public void testIncorrectWeight() {
        Item item = Item.createFromString("1,1 10000");
    }

}