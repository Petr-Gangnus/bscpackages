package bsc.homework.snailmail;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Map;

@Test
public class ReportItemsTests {
    @Test
    public void reportsForEmptyListTest(){
        Fees fees = new Fees(null);
        ReportItems items = new ReportItems(new LinkedList<Item>(), fees);
        Assert.assertEquals(items.getMap().size(), 0, "the report should be empty. (variant without fees). \n");
        fees = new Fees("src/main/resources/fees.txt");
        items = new ReportItems(new LinkedList<Item>(), fees);
        Assert.assertEquals(items.getMap().size(), 0, "the report should be empty. (variant with fees). \n");
    }
    @Test
    public void reportsWithoutFeesOneOfficeTest() throws Exception {
        Fees fees = new Fees(null);
        LinkedList<Item> items = new LinkedList<Item>();
        items.add(Item.createFromString("0.01 10000"));
        items.add(Item.createFromString("0.02 10000"));
        ReportItems reportItems = new ReportItems(items, fees);
        Map<Office, ReportItem> map = reportItems.getMap();
        Assert.assertEquals(map.size(), 1, "the items should be combined into 1 line for 1 office. \n");
        Assert.assertEquals(map.get(Office.createFromString("10000")).getSumWeights(), new BigDecimal("0.03"), "0.01+0.02=0.03 \n");

    }
    @Test
    public void reportsWithoutFeesTwoOfficeTest() throws Exception {
        Fees fees = new Fees(null);
        LinkedList<Item> items = new LinkedList<Item>();
        items.add(Item.createFromString("0.01 10000"));
        items.add(Item.createFromString("0.02 20000"));
        ReportItems reportItems = new ReportItems(items, fees);
        Map<Office, ReportItem> map = reportItems.getMap();
        Assert.assertEquals(map.size(), 2, "the items should be combined into 2 lines for 2 offices. \n");
        Assert.assertEquals(map.get(Office.createFromString("10000")).getSumWeights(), new BigDecimal("0.01"), "0.01+0=0.01 \n");
        Assert.assertEquals(map.get(Office.createFromString("20000")).getSumWeights(), new BigDecimal("0.02"), "0.02+0=0.02 \n");

    }

}