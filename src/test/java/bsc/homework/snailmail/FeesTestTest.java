package bsc.homework.snailmail;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;

@Test
public class FeesTestTest {
    @Test
    public void nullFeeTest() {
        Fees fees = new Fees(null);
        Assert.assertFalse(fees.turnedOn(), "fees should not be on. \n");
        Assert.assertNull(fees.getFeeForWeight(new BigDecimal("1")), "fees should return null weight. \n");
    }
    @Test
    public void checkFeesForWeightsTest() {
        Fees fees = new Fees("src/main/resources/fees.txt");
        Assert.assertTrue(fees.turnedOn(), "fees should be on. \n");
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("0")), new BigDecimal("1"));
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("0.1")), new BigDecimal("4"));
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("1")), new BigDecimal("20"));
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("10")), new BigDecimal("100"));
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("100")), new BigDecimal("500"));
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("1000")), new BigDecimal("500"));
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("0.001")), new BigDecimal("1"));
        Assert.assertEquals(fees.getFeeForWeight(new BigDecimal("99")), new BigDecimal("100"));
    }

}