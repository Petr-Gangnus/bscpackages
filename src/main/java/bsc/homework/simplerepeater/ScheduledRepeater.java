package bsc.homework.simplerepeater;

import java.util.concurrent.Callable;

import static java.lang.Thread.*;

/**
 * The ScheduledExecutorService being based on bugged DelayedWorkQueue, is also bugged
 * So, remaining in 1.6(and even in 1.7) version limits, for simple repetition it is better to create a handmade simple repeater.
 */
public class ScheduledRepeater {
    private final Callable<Boolean> callable;
    private final long startDelay;
    private final long interval;

    /**
     *
     * @param callable - The repeated code. When returns false, repeating should stop.
     * @param startDelay - The delay from start to the first call of {@code callable}, in millis
     * @param interval - The delay after end of {@code callable} to the next call, in millis
     */
    public ScheduledRepeater(Callable<Boolean> callable, long startDelay, long interval) {
        this.callable = callable;
        this.startDelay = startDelay;
        this.interval = interval;
    }

    /**
     *
     * @throws Exception can happen as a result of call or an interruption of the thread
     */
    public void start() throws Exception {
        sleep(startDelay);
        while(true) {
            sleep(interval);
            if (!callable.call()) return;
        }
    }
}
