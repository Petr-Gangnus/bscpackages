package bsc.homework.cleverstreams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * The input can be switched from file to System.in, for testing
 * For threading, if it is reading from the file, it can be asked, if closed (Scanner has not such method)
 */
public class InputScan {
    Scanner scanner;
    private boolean itemsAreReadFromFile;
    private boolean closed;
    /**
     * Set scanner for reading packages
     * If the {@code packagesFileName} is null or empty or lacks the real file, the scanner will read from the System.in
     * If not, the scanner will read from the {@code packagesFileName}
     */
    public InputScan(String packagesFileName) {
        this.scanner = new Scanner(System.in);
        itemsAreReadFromFile = false;
        closed = false;
        if (packagesFileName == null || packagesFileName.isEmpty()){
            return;
        }
        File file = new File(packagesFileName);
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        itemsAreReadFromFile = true;
    }

    public boolean gotItemsAreFromFile() {
        return itemsAreReadFromFile;
    }

    public boolean isClosed() {
        return closed;
    }

    public void close() {
        scanner.close();
        closed = true;
    }

    public boolean hasNextLine() {
        return scanner.hasNextLine();
    }

    public String nextLine() {
        return scanner.nextLine();
    }
}
