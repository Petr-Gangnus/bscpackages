package bsc.homework.cleverstreams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;


/**
 * The output can be switched from System.out to a file,
 * for the testing.
 */
public class CleverOutputStream {
    PrintStream stream;

    public CleverOutputStream(String outputStreamName){
        stream = System.out;
        if (outputStreamName == null || outputStreamName.isEmpty()){
            return;
        }
        File file = new File(outputStreamName);
        try {
            stream = new PrintStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void flush(){
        stream.flush();
    }

    public void println(String output){
        stream.println(output);
    }


}
