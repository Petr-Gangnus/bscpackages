package bsc.homework.snailmail;

import bsc.homework.cleverstreams.CleverOutputStream;
import bsc.homework.cleverstreams.InputScan;
import bsc.homework.simplerepeater.ScheduledRepeater;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * Reporting the info, collected in the {@code collected} map
 */
public class Reporter {
    static final Locale LOCALE = Locale.US;
    static final DecimalFormat DEC_F_2 = getDecimalFormatForPatternUsingLocale("#0.00");
    static final DecimalFormat DEC_F_3 = getDecimalFormatForPatternUsingLocale("#0.000");
    private final CleverOutputStream outputStream;
    private final AllItems allItems;
    private final long outputInterval;
    private final InputScan inputScan;
    private final Fees fees;


    public Reporter(Distributor distributor, long outputInterval){
        inputScan = distributor.getInputScan();
        outputStream = distributor.getOutputStream();
        allItems = distributor.getItems();
        fees = distributor.getFees();
        this.outputInterval = outputInterval;
    }

    private void reportByOffice(){
        List<Item> items = allItems.getAllTillEnd();
        ReportItems reportItems = new ReportItems(items, fees);
        Map<Office, ReportItem> map = reportItems.getMap();

        for (Map.Entry<Office, ReportItem> entry : map.entrySet()) {
            String startLine = entry.getKey() + " " + DEC_F_3.format(entry.getValue().getSumWeights());
            if (!fees.turnedOn()) {
                outputStream.println(startLine);
            } else {
                outputStream.println(startLine + " " + DEC_F_2.format(entry.getValue().getSumFees()));
            }
            outputStream.flush();
        }
    }




    public void repeatedReport() {
        ScheduledRepeater repeater = new ScheduledRepeater(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                    reportByOffice();
                    if (inputScan.isClosed()) {
                        // Output is finished;
                        return false;
                    }

                    return true;
            }
        }, 1, outputInterval);
        try {
            repeater.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static DecimalFormat getDecimalFormatForPatternUsingLocale(String pattern){
        NumberFormat nf = NumberFormat.getNumberInstance(LOCALE);
        DecimalFormat formatter = (DecimalFormat) nf;
        formatter.applyPattern(pattern);
        return formatter;
    }
}
