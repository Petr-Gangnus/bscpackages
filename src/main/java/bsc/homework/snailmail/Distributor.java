package bsc.homework.snailmail;

import bsc.homework.cleverstreams.CleverOutputStream;
import bsc.homework.cleverstreams.InputScan;

import java.util.*;

/**
 * The main class for collecting information about items sent by snailmail and reporting the collected info
 * The class contains switches that control where the data will be taken and written and what data will be used
 */
public class Distributor {
    /**
     * The source of data on snailmail items - file or System.in
     */
    private InputScan inputScan;
    /**
     * The target of report output
     */
    private CleverOutputStream outputStream;
    /**
     * The table of fees for items' weights
     */
    private Fees fees;

    static final long DEFAULT_OUTPUT_INTERVAL = 60000L; // 1 min

    /**
     * The sent items list
     */
    private final AllItems items = new AllItems();

    /**
     *
     * @param args Only the first arg is read that is interpreted as a source for fees/weight info
     */
    public static void main(String[] args) {
        String feesFileName = null;
        if(args.length>0 && !args[0].isEmpty()){
            feesFileName = args[0];
        }
        System.out.println("fees file=" + feesFileName);
        Distributor distributor = Distributor.create(feesFileName, null, null);
        distributor.launchThreads(0, DEFAULT_OUTPUT_INTERVAL);
    }

    static Distributor create(String feesFileName, String inputItemsFileName, String outputStreamName) {
        Locale.setDefault(Locale.US); // without that, weights and fees as 1.1 could be expected as 1,1 on Czech computers
        Distributor distributor = new Distributor();
        distributor.setFees(new Fees(feesFileName));
        distributor.setInputScan(new InputScan(inputItemsFileName));
        distributor.setOutputStream(new CleverOutputStream(outputStreamName));

        return distributor;
    }

    /**
     * Launch registering and reporting threads
     * @param inputInterval interval between line reading, if the reading is from a file. In ms
     * @param outputInterval Interval between report outputs. In ms.
     */
    void launchThreads(int inputInterval, long outputInterval){
        // Async Input
        Registrator registrator = new Registrator(this, inputInterval);
        registrator.start();
        // Scheduled output
        Reporter reporter = new Reporter(this, outputInterval);
        reporter.repeatedReport();
    }


    public void setInputScan(InputScan inputScan) {
        this.inputScan = inputScan;
    }

    private void setOutputStream(CleverOutputStream outputStream){
        this.outputStream = outputStream;
    }

    public boolean gotItemsAreFromFile() {
        return inputScan.gotItemsAreFromFile();
    }


    public InputScan getInputScan() {
        return inputScan;
    }

    public CleverOutputStream getOutputStream() {
        return outputStream;
    }

    public Fees getFees() {
        return fees;
    }

    private void setFees(Fees fees) {
        this.fees = fees;
    }

    public AllItems getItems() {
        return items;
    }

}
