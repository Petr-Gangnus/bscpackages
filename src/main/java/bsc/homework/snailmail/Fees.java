package bsc.homework.snailmail;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.*;

public class Fees {
    /**
     * The table of fees for items' weights
     */
    private Map<BigDecimal, BigDecimal> fees ;


    /**
     * Reading fees info from  file and putting it into the map
     * If there no file is expected, the fees map will be empty
     * If the file is not found, the exception stack will be output as warning,
     *     and the fees map will be empty.
     */
    public Fees(String feesFileName){
        // LinkedHashMap preserves the order
        fees  = new LinkedHashMap<BigDecimal, BigDecimal>();
        Scanner feesScan;
        if(feesFileName == null || feesFileName.isEmpty()) {
            return;
        }
        File file = new File(feesFileName);
        try {
            feesScan = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        String pattern = "\\s+";
        try {
            while (feesScan.hasNextLine()) {
                String[] splitString = feesScan.nextLine().split(pattern);
                fees.put(new BigDecimal(splitString[0]), new BigDecimal(splitString[1]));
            }
        }  catch (Exception e) {
            // problems by scanning or BigDecimal reading end here
            e.printStackTrace();
        } finally {
            feesScan.close();
        }

        reverseFees();
    }

    /**
     * Reverse the order or the fees map
     */
    private void reverseFees() {
        // reverse fees
        Map<BigDecimal, BigDecimal> fees2 = new LinkedHashMap<BigDecimal, BigDecimal>();

        List<Map.Entry<BigDecimal,BigDecimal>> list = new ArrayList<Map.Entry<BigDecimal,BigDecimal>>(fees.entrySet());
        for( int i = fees.size() -1; i >= 0 ; i --){
            Map.Entry<BigDecimal,BigDecimal> entry = list.get(i);
            fees2.put(entry.getKey(), entry.getValue());
        }
        fees=fees2;
    }

    BigDecimal getFeeForWeight(BigDecimal weight){
        if(fees.isEmpty()) {
            return null;
        }
        BigDecimal fee = null;
        for (Map.Entry<BigDecimal, BigDecimal> entry : fees.entrySet()){
            if (entry.getKey().compareTo(weight)>0) return fee;
            fee = entry.getValue();
            if (entry.getKey().compareTo(weight)==0) return fee;
        }
        return fee;
    }
    boolean turnedOn(){
        return fees != null && fees.size() != 0;
    }
}
