package bsc.homework.snailmail;

import java.util.*;

public class AllItems {
    private final Map<Integer, Item> fullMap = new LinkedHashMap<Integer, Item>();
    /**
     * This is the first key for which the map item was not exported yet
     */
    private int outputIndex = 0;

    /**
     * This is the export:
     * @return as a list all items that were not exported yet
     */
    public synchronized List<Item> getAllTillEnd(){
        List<Item> smallList = new LinkedList<Item>();
        while(outputIndex < fullMap.size()){
            smallList.add(fullMap.get(outputIndex));
            outputIndex++;
        }
        return smallList;
    }

    /**
     * Adding {@code item} to the end, for to be exported in future.
     * @param item registering the object form of a new input item
     */
    public synchronized void append(Item item){
        if(item != null) {
            fullMap.put(fullMap.size(), item);
        }
    }
}
