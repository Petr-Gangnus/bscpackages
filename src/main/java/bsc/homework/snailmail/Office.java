package bsc.homework.snailmail;

/**
 * Just now the post office has only info about its pid code
 * Eventually here could be much more information
 */
public class Office implements Comparable<Office>{

    String pid;

    private Office(){}

    public static Office createFromString(String pid) throws Exception {
        Office snailmailOffice = new Office();
        snailmailOffice.setPid(pid);
        return snailmailOffice;
    }

    public String toString() {
        return pid;
    }

    public void setPid(String pid) throws Exception {
        if (pid == null) {
            throw new Exception("null pid");
        }
        if (!pid.matches("\\d{5}")) {
            throw new Exception("'" + pid + "'" + " - incorrect pid format");
        }

        this.pid = pid;
    }
    //--------------------------------------------------------------------------------
    // As the post office will be used as a key in maps, it should be correctly hashed
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        return this.pid.equals(((Office) obj).pid);
    }

    @Override
    public int hashCode() {
        return this.pid.hashCode();
    }

    @Override
    public int compareTo(Office o) {
        return this.toString().compareTo(o.toString());
    }
}
