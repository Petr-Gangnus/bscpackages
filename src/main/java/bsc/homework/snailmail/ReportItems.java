package bsc.homework.snailmail;

import java.util.*;

public class ReportItems {
    private final List<Item> items;
    private final List<Office> offices;
    private final Map<Office, ReportItem> map;
    private final Fees fees;

    public ReportItems(List<Item> items, Fees fees){
        this.fees = fees;
        this.items = items;

        // Create maps with summed content for report
        offices = createOfficesForItems();
        map = createMapForOfficesAndItems();
        sumItemsIntoMap();

    }

    private List<Office>  createOfficesForItems( ){
        List<Office> offices = new ArrayList<Office>();
        for (Item item : items) {
            if(!offices.contains(item.getOffice())){
                offices.add(item.getOffice());
            }
        }
        Collections.sort(offices);
        return offices;
    }

    private Map<Office, ReportItem> createMapForOfficesAndItems() {
        Map<Office, ReportItem> map = new LinkedHashMap<Office, ReportItem>();
        for (Office office : offices) {
            map.put(office,new ReportItem(fees));
        }
        return map;
    }

    private void sumItemsIntoMap(){
        for(Item item : items){
            ReportItem reportItem = map.get(item.getOffice());
            map.put(item.getOffice(),reportItem.addItem(item));
        }
    }

    public Map<Office, ReportItem> getMap(){
        return map;
    }

}
