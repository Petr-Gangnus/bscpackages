package bsc.homework.snailmail;

import bsc.homework.cleverstreams.InputScan;

/**
 * Reading input lines and collecting information from them for separate post offices
 */
class Registrator extends Thread {
    private final int inputInterval;
    private final InputScan inputScan;
    private final AllItems items;
    private final boolean gotItemsAreFromFile;

    public Registrator(Distributor distributor, int inputInterval) {
        inputScan = distributor.getInputScan();
        items = distributor.getItems();
        gotItemsAreFromFile = distributor.gotItemsAreFromFile();
        this.inputInterval = inputInterval;
    }

    public void run() {

        while(inputScan.hasNextLine() || !inputScan.gotItemsAreFromFile()) {
            String line = inputScan.nextLine();
            // reading comments or empty lines
            // lines that are empty or start with #, should be missed, with zero waiting
            if (line.isEmpty() || "#".equals(line.substring(0, 1))) continue;

            // set time interval
            // if an input line contains the third position, it is the current time interval in ms
            // if not, then the default input interval is chosen
            // thus we can test the behaviour of threads at different timing
            String[] splitString = line.split("\\s+");
            int interval = inputInterval;
            if (gotItemsAreFromFile && splitString.length > 2) {
                interval = Integer.parseInt(splitString[2]);
            }

            Item snailmailItem = Item.createFromString(line);
            if(snailmailItem != null) {
                items.append(snailmailItem);
            }

            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                // normally the thread should not be interrupted
                e.printStackTrace();
            }
        }
        inputScan.close();
    }

}
