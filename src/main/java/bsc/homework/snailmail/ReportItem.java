package bsc.homework.snailmail;

import java.math.BigDecimal;

/**
 * Summarizing information for some snailmail Items
 */
public class ReportItem {
    private BigDecimal sumWeights = new BigDecimal(0);
    private BigDecimal sumFees = new BigDecimal(0);
    private final Fees fees;

    public ReportItem(Fees fees){
        this.fees = fees;
    }

    ReportItem addItem(Item item){
        // the weight will be summed up for every post office
        sumWeights = sumWeights.add(item.getWeight());
        // If fees map is not empty in the distributor, fees will be summed up for every post office, too
        if (fees.turnedOn()) {
            // if the fees info exists, the money will be summed for every post
            sumFees = sumFees.add(fees.getFeeForWeight(item.getWeight()));
        }

        return this;
    }

    public BigDecimal getSumWeights() {
        return sumWeights;
    }

    public BigDecimal getSumFees() {
        return sumFees;
    }

}
