package bsc.homework.snailmail;

import java.math.BigDecimal;

/**
 * The item that will be registered as sent by snailmail
 */
public class Item {
    private Office office;
    private BigDecimal weight;

    private Item(){}

    public static Item createFromString(String inputLine){
        Item snailmailItem = new Item();
        String[] splitString = inputLine.split("\\s+");

        snailmailItem.weight = new BigDecimal(splitString[0]);
        try {
            snailmailItem.office = Office.createFromString( splitString[1]);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return snailmailItem;
    }

    public Office getOffice() {
        return office;
    }

    public BigDecimal getWeight() {
        return weight;
    }

}
